package biz.picosoft.springsecurityyoussfi.service;


import biz.picosoft.springsecurityyoussfi.entity.AppRole;
import biz.picosoft.springsecurityyoussfi.entity.AppUser;
import biz.picosoft.springsecurityyoussfi.repository.AppRoleRepository;
import biz.picosoft.springsecurityyoussfi.repository.AppUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AccountServiceImp implements AccountService {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private AppUserRepository appUserRepository;

    @Autowired
    private AppRoleRepository appRoleRepository;

    @Override
    public AppUser saveUser(AppUser user) {
        String hashPW = bCryptPasswordEncoder.encode(user.getPassword());
        user.setPassword(hashPW);
        return appUserRepository.save(user);
    }

    @Override
    public AppRole saveRole(AppRole role) {
        return appRoleRepository.save(role);
    }

    @Override
    public void AddRoleToUser(String username, String roleName) {
        AppRole role = appRoleRepository.findByRole(roleName);
        AppUser user = appUserRepository.findByUsername(username);
        user.getRoles().add(role);
        appUserRepository.save(user);
    }

    @Override
    public AppUser findUserByUsername(String username) {
        return appUserRepository.findByUsername(username);
    }
}
