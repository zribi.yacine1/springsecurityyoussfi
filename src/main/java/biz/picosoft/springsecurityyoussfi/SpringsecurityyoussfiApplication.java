package biz.picosoft.springsecurityyoussfi;

import biz.picosoft.springsecurityyoussfi.entity.AppRole;
import biz.picosoft.springsecurityyoussfi.entity.AppUser;
import biz.picosoft.springsecurityyoussfi.service.AccountService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class SpringsecurityyoussfiApplication  implements CommandLineRunner {

	@Autowired
	private AccountService accountService;

	public static void main(String[] args) {
		SpringApplication.run(SpringsecurityyoussfiApplication.class, args);
	}

	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("****************************** START ******************************************");
		AppUser user1 = new AppUser(null, "yacinezr", "123456789", null);
		AppUser user2 = new AppUser(null, "yacinezr2", "123456789", null);
		AppUser user3 = new AppUser(null, "yacinezr3", "123456789", null);

		AppRole role1 = new AppRole(null, "admin");
		AppRole role2 = new AppRole(null, "user");

		accountService.saveUser(user1);
		accountService.saveUser(user2);
		accountService.saveUser(user3);

		accountService.saveRole(role1);
		accountService.saveRole(role2);

		accountService.AddRoleToUser(user1.getUsername(), role1.getRole());
		accountService.AddRoleToUser(user1.getUsername(), role2.getRole());
		accountService.AddRoleToUser(user2.getUsername(), role2.getRole());
		accountService.AddRoleToUser(user3.getUsername(), role2.getRole());
	}
}
