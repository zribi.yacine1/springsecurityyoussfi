package biz.picosoft.springsecurityyoussfi.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
//@Import(SecurityProblemSupport.class)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    // ici on definie le type d'authentification, par ldap, base de donnée ou en mémoire
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.inMemoryAuthentication().passwordEncoder(NoOpPasswordEncoder.getInstance()).withUser("admin").password("admin").roles("ADMIN", "USER");
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
//        http.formLogin(); //pour utiliser jwt il faut deactiver formlogin pour ne pas generer la session
//        de systeme d'authentification par reference (cookie a une reference vers une session)
//        à un systeme d'authentification par valeur
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.authorizeRequests().antMatchers("/register/**").permitAll();
        http.authorizeRequests().antMatchers(HttpMethod.POST, "/api/ValidateFile").hasAuthority("admin");
        http.authorizeRequests().anyRequest().authenticated();
        //on ajoute ici les filtre
        // ce filtre JWTAuthenticationFilter nous avons developper et on lui passe comme paramétre authenticationManager() qu'on
        // l'hérite de WebSecurityConfigurerAdapter de spring securité, authenticationManager() nous permermetons de retourné
        // l'objet d'authentification Principal de spring sec
        http.addFilter(new JWTAuthenticationFilter(authenticationManager()));

        // avant toutes requete executer ce filtre
        // filtre baser sur le username et password UsernamePasswordAuthenticationFilter.class
        http.addFilterBefore(new JWTAutorizationFilter(),UsernamePasswordAuthenticationFilter.class);
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                .antMatchers(HttpMethod.OPTIONS, "/**")
                .antMatchers("/app/**/*.{js,html}")
                .antMatchers("/i18n/**")
                .antMatchers("/content/**")
                .antMatchers("/swagger-ui/index.html")
                .antMatchers("/test/**")
                .antMatchers("/h2-console/**");
    }
}
